#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int writeLeds(unsigned int mask);

int main(int argc, char **argv) {

	int count = 0;
	char c = 0;

	while (c != 'q') {
		writeLeds(count++ % 8);
		fprintf(stderr, "Press ENTER to keep counting: \n");
		c = getchar();
	}

	
	return 0;

}

int writeLeds(unsigned int mask) {

	int bytes = 3;
	int device;
	char *buff = malloc(sizeof(char) * 3);


	if (mask < 0 || mask > 7) 
		return -1;

	switch (mask) {
		case 0:
			buff = "000";
			break;
		case 1: 
			buff = "001";
			break;
		case 2:
			buff = "020";
			break;
		case 3:
			buff = "021";
			break;
		case 4:
			buff = "300";
			break;
		case 5:
			buff = "301";
			break;
		case 6:
			buff = "320";
			break;
		case 7:
			buff = "321";
			break;
	}

	fprintf(stderr, "printing... %s\n", buff);

	if ((device = open("/dev/chardev_leds", O_WRONLY|O_TRUNC)) == -1) {
		fprintf(stderr, "File not opened properly...\n");
		return -1;
	}

	write(device, buff, bytes);

	close(device);


	return 1;

}

