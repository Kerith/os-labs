#include "sched.h"

static task_t* pick_next_task_fcfs(runqueue_t* rq, int cpu) {

	task_t* t=head_slist(&rq->tasks);

    if (t) {
        /* Current is not on the rq*/
        remove_slist(&rq->tasks,t);
        t->on_rq=FALSE;
        rq->cur_task=t;
    }
    return t;
}
/* Given a task, put it into a reun queue */
static void enqueue_task_fcfs(task_t* t, int cpu, int runnable) {

	//Get the run queue
	runqueue_t* rq = get_runqueue_cpu(cpu);
	//If the task is already on the cpu, or it's idle, don't put it in.
	if (t->on_rq || is_idle_task(t))
        return;
    //From now on, the task HAS to go into the CPU
    insert_slist(&rq->tasks,t); //Push task
    t->on_rq=TRUE;

    //If the task was not runnable before on this RQ (just changed the status)
    if (!runnable) {
        rq->nr_runnable++; //Increase the number of runnable tasks in the queue..
        t->last_cpu=cpu; //Update the CPU.
    }
}

/* Implements what happens when a task has completed a time unit (preemption, etc...) */
static void task_tick_fcfs(runqueue_t* rq, int cpu) {
	//Get the task
	task_t* current = rq->cur_task;

	if (is_idle_task(current)) {
		return;
	}

	//Check if it has completed
	if (current->runnable_ticks_left == 1) {
		rq->nr_runnable--; //Processes in "ready" or "running", not ALL processes
	}	
}


static task_t* steal_task_fcfs(runqueue_t* rq, int cpu) {

	task_t* t = tail_slist(&rq->tasks);

	if (t) { //We got a task to steal
		remove_slist(&(rq->tasks), t);
		t->on_rq=FALSE;
		rq->nr_runnable--;
	}

	return t;
}


sched_class_t fcfs_sched= {
    //We don't need new and destroy since we don't have anything to put in the tcs_data
    .pick_next_task=pick_next_task_fcfs,
    .enqueue_task=enqueue_task_fcfs,
    .task_tick=task_tick_fcfs,
    .steal_task=steal_task_fcfs
};