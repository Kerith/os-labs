#!/bin/bash

# Clear the mess
make clean
make
rm -r results
mkdir results

# Read the CPU count and verify
echo "How many CPUs? (max. 8)"
read NUM_CPUS
while [ $NUM_CPUS -gt 8 ]; do
	echo "Please insert 8 at most. It's not rocket surgery, try again."
	read NUM_CPUS
done

# Read the input file and verify it
echo -e "What is the input file? "
#read INPUT_FILE
while [ ! -f "$INPUT_FILE" ]; do
	echo "Please input a valid regular file."
	read INPUT_FILE
done

# For each of the available schedulers...
for sched in $( ./schedsim -L ); do
	if [ $sched != 'Available' ] && [ $sched != 'schedulers:' ]; then 
		# Execute the scheduler
		echo "$sched"
		for cpu in $(seq "$NUM_CPUS"); do
			mkdir "results/$sched-$cpu"
			# Simulate stuff
			./schedsim -n "$cpu" -i "$INPUT_FILE" -s "$sched" -p
			for i in $(seq "$cpu"); do				
				# Move files
				mv CPU_"$(( i - 1 ))".log results/"$sched-$cpu"/"$sched"-CPU-"$(( i - 1 ))".log
			done			
		done
	fi
done

# Generate graphs
cd ../gantt-gplot
for fn in $( ls ../schedsim/results ); do
	for log in $( ls "../schedsim/results/$fn" ); do
		./generate_gantt_chart "../schedsim/results/$fn/$log"
	done
done
#./generate_gantt_chart
cd ../schedsim


